<?php

/**
 * @author      José Lorente <jl@bytebox.es>
 * @license     <http://opensource.org/licenses/gpl-license.php> GNU Public License
 * @copyright   José Lorente
 * @version     1.0
 */

namespace custom\bootstrap;

use \yii\bootstrap\Widget as BootstrapWidget;

/**
 *
 * @author José Lorente <jose.lorente.martin@gmail.com>
 */
class Widget extends BootstrapWidget {
    
}
