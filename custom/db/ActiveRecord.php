<?php

/**
 * @author      Jose Lorente <jlorente@vivocom.eu>
 * @license     <http://opensource.org/licenses/gpl-license.php> GNU Public License
 * @copyright   José Lorente
 * @version     1.0
 */

namespace custom\db;

use yii\db\ActiveRecord as BaseActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * Custom implementation of  ActiveRecord
 *
 * @author José Lorente <jose.lorente.martin@gmail.com>
 */
class ActiveRecord extends BaseActiveRecord {

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            TimestampBehavior::className()
        ]);
    }

}
