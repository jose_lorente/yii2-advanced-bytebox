<?php

/**
 * @author      José Lorente <jl@bytebox.es>
 * @license     <http://opensource.org/licenses/gpl-license.php> GNU Public License
 * @copyright   José Lorente
 * @version     1.0
 * 

  namespace custom\web;

  use yii\web\Controller as YiiController;

  /**
 * Custom controller class to extend yii web controller with custom behaviors.
 * 
 * @author Jose Lorente <jose.lorente.martin@gmail.com>
 */
class Controller extends YiiController {
    
}
