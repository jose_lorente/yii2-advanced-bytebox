<?php

/**
 * @author      José Lorente <jose.lorente.martin@gmail.com>
 * @license     <http://opensource.org/licenses/gpl-license.php> GNU Public License
 * @copyright   José Lorente
 * @version     1.0
 */

namespace custom\assets\formAjaxUtils;

use yii\web\AssetBundle;

/**
 * Description of FormDataGetterAsset
 *
 * @author José Lorente <jose.lorente.martin@gmail.com>
 */
class FormAjaxUtilsAsset extends AssetBundle {

    public $sourcePath = '@custom/assets/formAjaxUtils/js';
    public $js = [
        'FormAjaxUtils.js',
    ];

}
